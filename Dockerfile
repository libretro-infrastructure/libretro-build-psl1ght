FROM ubuntu:latest

ENV DEBIAN_FRONTEND="noninteractive"

ARG uid
ARG branch=develop
ENV branch=$branch

RUN apt-get update && \
    apt-get install -y \
    autoconf \
    automake \
    bison \
    flex \
    gcc \
    libelf-dev make \
    texinfo \
    libncurses5-dev \
    patch \
    python3 \
    python3-distutils \
    subversion \
    wget \
    zlib1g-dev \
    libtool \
    libtool-bin \
    python3-dev \
    bzip2 \
    libgmp3-dev \
    pkg-config \
    g++ \
    libssl-dev \
    clang \
    unzip \
    cmake \
    make \
    bsdmainutils \
    curl \
    xxd \
    autotools-dev \
    perl \
    git \
    sudo \
    nvidia-cg-toolkit && \
    useradd -d /developer -m developer && \
    usermod -aG sudo developer && \
    chown -R developer:developer /developer && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/

ENV HOME=/developer
ENV PS3DEV=/opt/ps3dev
ENV PSL1GHT=$PS3DEV
ENV PATH=$PATH:$PS3DEV/bin:$PS3DEV/ppu/bin:$PS3DEV/spu/bin

RUN ln -s /usr/bin/python3 /usr/bin/python && \
    ln -s /usr/bin/python3-config /usr/bin/python-config

RUN git clone https://github.com/ps3dev/ps3toolchain --depth=1 && \
    cd ps3toolchain/ && chmod +x ./*.sh && \
    ./toolchain.sh && \
    cd .. && rm -fr ps3toolchain/

RUN git clone https://github.com/ps3dev/PSL1GHT --depth=1 && \
    cd PSL1GHT/ && \
    make install-ctrl && make && make install && \
    cd .. && rm -fr PSL1GHT/

RUN git clone https://github.com/ps3dev/ps3libraries --depth=1 && \
    cd ps3libraries/ && chmod +x ./*.sh && \
    ./libraries.sh && \
    cd .. && rm -fr ps3libraries/

USER developer
WORKDIR /developer
VOLUME /developer

CMD make
